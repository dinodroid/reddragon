# If not running interactively, don't do anything
# /\/|  ___              | |
#|/\/  / / |__   __ _ ___| |__  _ __ ___
#     / /| '_ \ / _` / __| '_ \| '__/ __|
#    / /_| |_) | (_| \__ \ | | | | | (__
#   /_/(_)_.__/ \__,_|___/_| |_|_|  \___|


#--------------------------> SET VARIABLES <--------------------------------

# Default Apps
export TERMINAL="alacritty"
export EDITOR="micro"
export VISUAL="codium"
export BROWSER="brave --incognito"
export TRUE_BROWSER="brave --incognito"
export VIDEO="mpv"
export IMAGE="sxiv -a"
export COLORTERM="truecolor"
export OPENER="linkhandler"
export PAGER="less"
export WM="qtile"

# Other XDG paths
export XDG_DATA_HOME=${XDG_DATA_HOME:="$HOME/"}
export XDG_CACHE_HOME=${XDG_CACHE_HOME:="$HOME/.cache"}
export XDG_CONFIG_HOME=${XDG_CONFIG_HOME:="$HOME/.config"}

# PATH OF MY SCRIPTS
export PATH="$PATH:$HOME/.scripts"

#-----------------------=> BASH ALIASES <=------------------------------------

# Always Same Option Aliases
## Add color
alias ls='ls -alhC --color=always --group-directories-first'
alias grep='grep --color=auto'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'
#alias pgrep='pgrep --color=auto'
alias mkdir='mkdir -pv'
alias df='df -h --total'
alias du='du -h --total | sort -h'
alias free='free -gh'
alias ps='ps aux'
alias journalctl='sudo journalctl -p 3 -xb'
alias ping='ping -c 5'

## Add Interactive Check
alias rm='rm -I' ## Less Intrusive
alias mv='mv -i'
alias cp='cp -i'

# New aliases
alias cls='clear && neofetch --ascii "$(fortune -as |cowsay -f bunny)" --disable gpu shell resolution de wm theme icons term --color_blocks off'
alias wall="cat ~/.cache/wal/sequences"
alias ..='cd ..'
alias fname='find . -name'
alias mirrors='sudo reflector --latest 10 --sort rate --save /etc/pacman.d/mirrorlist > /dev/null 2>&1 && notify-send "Mirrors updated."'
alias pkg-refresh='sudo pacman-key --refresh-keys && notify-send "Packages Keys Refreshed."'
alias pkg-clean='sudo pacman -Rns $(pacman -Qtdq)' # Remove orphan packages
alias killps='kill -9'

## Shortening
alias pkg='sudo pacman'
alias sys='sudo systemctl'
alias vpn-start='sudo protonvpn c -f > /dev/null 2>&1 && notify-send "Proton VPN Connected"'
#alias rofi='rofi -icon-theme "Sea" -font "Fantasque Sans Mono 12" -show-icons'
alias vpn-stop='sudo protonvpn d > /dev/null 2>&1 && notify-send "Proton VPN Disconnected"'

# Change Programs
alias vim='nvim'
alias nano='micro'

# Combine Programs
alias mpv='devour mpv'

# External Alias
alias ip='curl https://ipinfo.io/ip'
alias weather='curl https://wttr.in'

# Youtube DL
alias ytdlmp3list="youtube-dl -i --extract-audio --embed-thumbnail --add-metadata --audio-format 'mp3' -o '~/Music/%(playlist_title)s/%(title)s.%(ext)s'"

#------------------------------> BASH FUNCTIONS  <------------------------------------

# New functions
mkcd () { mkdir -p -- "$1" && cd -P -- "$1"; }
mkscript() { echo '#!/bin/bash' > "$1" && sudo chmod u+x "$1" && $EDITOR "$1"; }
phpserve() { php -S 0.0.0.0:$1 -t $2 & }
cheat(){ curl cheat.sh/"$@"; }
restart_app() { killall "$1" && $1 &  }
ex ()
{
  if [ -f $1 ] ; then
    case $1 in
      *.tar.bz2)   tar xjf $1   ;;
      *.tar.gz)    tar xzf $1   ;;
      *.bz2)       bunzip2 $1   ;;
      *.rar)       unrar x $1   ;;
      *.gz)        gunzip $1    ;;
      *.tar)       tar xf $1    ;;
      *.tbz2)      tar xjf $1   ;;
      *.tgz)       tar xzf $1   ;;
      *.zip)       unzip $1     ;;
      *.Z)         uncompress $1;;
      *.7z)        7z x $1      ;;
      *.deb)       ar x $1      ;;
      *.tar.xz)    tar xf $1    ;;
      *.tar.zst)   unzstd $1    ;;
      *)           echo "'$1' cannot be extracted via ex()" ;;
    esac
  else
    echo "'$1' is not a valid file"
  fi
}

#---------------------------------------------------------------------------------------------

[[ $- != *i* ]] && return

# Disables CTRL+S and CTRL+Q
stty -ixon

# Allows to Cd if accidently typed directory name
shopt -s autocd

# Bash prompt

PS1="\n\[$(tput sgr0)\]\[$(tput bold)\]\[\033[38;5;7m\]\u\[$(tput sgr0)\] \[$(tput sgr0)\]\[$(tput bold)\]\[\033[38;5;5m\][\[$(tput sgr0)\] \w \[$(tput sgr0)\]\[$(tput bold)\]\[\033[38;5;5m\]]\[$(tput sgr0)\] \[$(tput sgr0)\]\[$(tput bold)\]\[\033[38;5;7m\]\\n\$\[$(tput sgr0)\] \[$(tput sgr0)\]"

wall && clear
