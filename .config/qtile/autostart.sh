#!/bin/bash

function run {
    if ! pgrep $1;
    then
        $@&
    fi
}

# Turn on Tap to Click
run ~/.scripts/taptoclick &

# Set Wallpaper
#run xwallpaper --zoom ~/Pictures/wall.png &
run feh --no-fehbg --bg-scale '/home/dinodroid/Pictures/wall.png' &

# Run Systray Programs
run nm-applet &

# Run polkit
run lxsession &

# Run xfce powermanager
run xfce4-power-manager &

# Run dunst
run dunst > /dev/null 2>&1 &

# Run redshift
run redshift-gtk &

# Run compositor
run picom &
