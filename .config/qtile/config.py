#####################################
# Qtile Configuration
# ~/.config/qtile/config.py
####################################

from typing import List  # noqa: F401
from libqtile import bar, layout, widget, hook, qtile
from libqtile.config import Click, Drag, Group, Key, Screen
from libqtile.lazy import lazy
from libqtile.command import lazy
import os
import subprocess
from colors import init_colors

mod = "mod4"
home=os.path.expanduser('~')

## Colors-------------------------------------------------------------------

# COLORS FOR THE BAR
col =  init_colors();

fonts = [  
            "Fantasque Sans Mono Bold",   
            "Iosevka Nerd Font Bold"
    ]

## Commands------------------------------------------------------------------

class command:
    terminal        = 'alacritty'
    editor          = 'codium'
    explorer        = 'pcmanfm'
    browser         = 'brave'
    windowmenu      = 'rofi -show window'
    runmenu         = 'rofi -show run'
    programmenu     = 'rofi -show drun'

    def script(scriptname):
        return os.path.join(home, '.scripts/'+ scriptname)

## Keymaps-------------------------------------------------------------------

keys = [
        # Switch between windows in current stack pane
        Key([mod], "Right",             lazy.layout.down(),         desc="Move focus down in stack pane"),
        Key([mod], "Left",              lazy.layout.up(),           desc="Move focus up in stack pane"),

        # Move windows up or down in current stack
        Key([mod, "control"], "Right",  lazy.layout.shuffle_down(), desc="Move window down in current stack "),
        Key([mod, "control"], "Left",   lazy.layout.shuffle_up(),   desc="Move window up in current stack "),

        Key([mod], "h", lazy.layout.grow(),lazy.layout.increase_nmaster(),    desc='Expand window (MonadTall), increase number in master pane (Tile)'),
        Key([mod], "l", lazy.layout.shrink(),lazy.layout.decrease_nmaster(),  desc='Shrink window (MonadTall), decrease number in master pane (Tile)'),

        # Switch window focus to other pane(s) of stack
        Key([mod, "shift"], "Tab",      lazy.layout.next(),         desc="Switch window focus to other pane(s) of stack"),

        # Swap panes of split stack
        Key([mod, "shift"], "space",    lazy.layout.rotate(),       desc="Swap panes of split stack"),
        Key([mod, "shift"], "Return",   lazy.layout.toggle_split(), desc="Toggle between split and unsplit sides of stack"),

        # Toggle between different layouts as defined below
        Key([mod], "space",             lazy.next_layout(),         desc="Toggle between layouts"),
        Key([mod], "q",                 lazy.window.kill(),         desc="Kill focused window"),

        # Move between groups
        Key([mod], 'Down',              lazy.screen.prev_group(),   desc="Move to previous group"),
        Key([mod], 'Up',                lazy.screen.next_group(),   desc="Move to next group"),

        Key([mod, "control"], "r",      lazy.restart(),             desc="Restart qtile"),
        Key([mod, "control"], "q",      lazy.shutdown(),            desc="Shutdown qtile"),

        # Basic Keymaps
        Key([],"XF86AudioMute",         lazy.spawn(command.script("togglemute")),     desc="Toggle Mute"),
        Key([],"XF86AudioRaiseVolume",  lazy.spawn(command.script("volumeup")),       desc="Volume Up"),
        Key([],"XF86MonBrightnessUp",   lazy.spawn(command.script("brightnessup")),   desc="Brightness Up"),
        Key([],"XF86MonBrightnessDown", lazy.spawn(command.script("brightnessdown")), desc="Brightness Down"),
        Key([],"XF86AudioLowerVolume",  lazy.spawn(command.script("volumedown")),     desc="Volume Down"),
        Key([],"Print",                 lazy.spawn(command.script("screenshotfull")), desc="Screenshot Full"),
        Key(["shift"],"Print",          lazy.spawn(command.script("screenshotsel")),  desc="Screenshot Selection"),

        # command Keymaps
        Key([mod], "Return",            lazy.spawn(command.terminal),                   desc="Launch Terminal"),
        Key([mod], "t",                 lazy.spawn(command.terminal),                   desc="Launch Terminal"),
        Key([mod], "c",                 lazy.spawn(command.editor),                     desc="Launch VSCode"),
        Key([mod], "e",                 lazy.spawn(command.explorer),                   desc="Launch Thunar"),
        Key([mod], "w",                 lazy.spawn(command.browser),                    desc="Launch Firefox"),
        Key([mod], "r",                 lazy.spawn(command.runmenu),                    desc="Launch Run"),
        Key([mod], "p",                 lazy.spawn(command.programmenu),                desc="Launch Drun"),
        Key([mod], "Tab",               lazy.spawn(command.windowmenu),                 desc="Switch Open Windows"),
        Key([mod], "s",                 lazy.spawn(command.script("streamclipboard")),  desc="Launch StreamClipboard"),
        Key([mod], "z",                 lazy.spawn(command.script("wallpaper")),        desc="Change Wallpaper"),
    ]

## Groups--------------------------------------------------------------------------------

groups = [Group(i) for i in "123"]

for i in groups:
    keys.extend([
        # mod1 + letter of group = switch to group
        Key([mod], i.name, lazy.group[i.name].toscreen(),
            desc="Switch to group {}".format(i.name)),

        # mod1 + shift + letter of group = switch to & move focused window to group
        Key([mod, "shift"], i.name, lazy.window.togroup(i.name, switch_group=True),
            desc="Switch to & move focused window to group {}".format(i.name)),
    ])

## Layouts-------------------------------------------------------------------------------------------------------

layout_defaults = dict(margin=20, border_width=5, border_focus=col[12][0], border_normal=col[3][0], grow_amount=5)

layouts = [
    layout.MonadTall(**layout_defaults),
    layout.Stack(num_stacks=2, **layout_defaults),
    layout.Max(**layout_defaults),
    layout.Matrix(**layout_defaults),
    # Try more layouts by unleashing below layouts.
    #layout.Bsp(), #layout.Columns(), #layout.MonadWide(), #layout.RatioTile(), #layout.Tile(), #layout.TreeTab(), #layout.VerticalTile(), #layout.Zoomy(),
]

## Widgets-------------------------------------------------------------------------------------------------------------------------------

widget_defaults = dict(font=fonts[1], fontsize=13, padding=5, background=col[2])

extension_defaults = widget_defaults.copy()

def icon(icon_text, color, backcolor):
    return widget.TextBox(font=fonts[1],text=icon_text,foreground=color,background=backcolor,padding = 3,fontsize=13)

def powerline(color1, color2):
    return widget.TextBox(fontsize=25,padding=0,text=' ',foreground=color1,background=color2)

def customscript(color1, color2, update, script, mousecallbacks):
    return widget.GenPollText(foreground=color1, background=color2, update_interval=update,
                func=lambda:subprocess.check_output(command.script(script)).decode("utf-8"), mouse_callbacks=mousecallbacks)

# Mouse Callbacks Functions

def runscript(script):
    return lambda:qtile.cmd_spawn(script)

screens = [
    Screen(
        top=bar.Bar(
            [
                widget.Sep(padding=10, foreground=col[2], background=col[2]),
                # Get Current Layout Icon
                widget.CurrentLayout(foreground=col[9]),

                # Get Groups 
                powerline(col[3], col[2]),
                widget.GroupBox(disable_drag=False,active=col[5],inactive=col[3],highlight_method ="text",
                    this_current_screen_border=col[8],foreground=col[1],background=col[3]),

                # Window Name
                powerline(col[2], col[3]),
                widget.WindowName(padding=5,font=fonts[0],fontsize=14,foreground = col[9],background = col[2]),

                powerline(col[4], col[2]), 
                icon(' ', col[8], col[4]), 
                customscript(col[0], col[4], 30, "pkgupdateavailable", {"Button1": runscript('pamac-manager')}),

                # Specs Indicators
                powerline(col[3], col[4]),
                icon(' ', col[8], col[3]), 
                customscript(col[12], col[3], 1, "cpupercent", {"Button1": runscript(command.terminal + ' -e htop') }),

                icon('', col[8], col[3]), 
                customscript(col[12], col[3], 1, "memused", {"Button1": runscript(command.terminal + ' -e htop') }),

                # System Indicators
                powerline(col[4], col[3]), 
                customscript(col[11], col[4], 1, "volumestatus", {"Button1" : runscript('pavucontrol') } ),
                customscript(col[11], col[4], 1, "batterystatus", {"Button1" : runscript('xfce4-power-manager-settings')} ),
                
                # Temperature and Date
                powerline(col[5], col[4]),
                icon('﨎 ', col[12], col[5]),
                customscript(col[0], col[5], 120, "temperature", {"Button1" : runscript(command.browser + ' "https://wttr.in/lucknow"') }),
                
                icon('﨟 ', col[12], col[5]), 
                customscript(col[0], col[5], 60, "clock", {"Button1": runscript(command.script('calender')) }), 

                # System Tray
                powerline(col[3], col[5]),
                widget.Systray(background=col[3]),
                widget.Sep(padding=10, foreground=col[3], background=col[3]),
            ],
            25, opacity=1, margin=[5,20,5,20]
        ),        
    ),
]

# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front())
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: List
main = None  # WARNING: this is deprecated and will be removed soon
follow_mouse_focus = False
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(float_rules=[
    # Run the utility of `xprop` to see the wm class and name of an X client.
    {'wmclass': 'confirm'},
    {'wmclass': 'dialog'},
    {'wmclass': 'download'},
    {'wmclass': 'error'},
    {'wmclass': 'file_progress'},
    {'wmclass': 'notification'},
    {'wmclass': 'splash'},
    {'wmclass': 'toolbar'},
    {'wmclass': 'confirmreset'},  # gitk
    {'wmclass': 'makebranch'},  # gitk
    {'wmclass': 'maketag'},  # gitk
    {'wname': 'branchdialog'},  # gitk
    {'wname': 'pinentry'},  # GPG key password entry
    {'wmclass': 'ssh-askpass'},  # ssh-askpass
    {'wmclass': 'Save As'},  # ssh-askpass
])
auto_fullscreen = True
focus_on_window_activation = "smart"

# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"

@hook.subscribe.startup_once
def autostart():
    home = os.path.expanduser('~/.config/qtile/autostart.sh')
    subprocess.call([home])
